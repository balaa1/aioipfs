image: "python:3.9"

stages:
  - build
  - pypi

variables:
  PATH: "/opt/ipfs/bin:/usr/bin:/bin:/sbin"

build:
  stage: build
  parallel:
    matrix:
      # This is the list of go-ipfs/kubo versions that
      # we run unit tests against

      - IPFSD_TEST_RELEASE: [
          go-ipfs-0.8.0,
          go-ipfs-0.9.0,
          go-ipfs-0.10.0,
          go-ipfs-0.11.0,
          go-ipfs-0.12.0,
          go-ipfs-0.13.0,
          kubo-0.14.0,
          kubo-0.15.0,
          kubo-0.16.0,
          kubo-0.17.0
        ]

  script:
    - apt-get update
    - apt-get install -qy --force-yes python3-pip python3-venv
    - mkdir -p /opt/ipfs/bin

    - |-
      # Match go-ipfs version number
      if [[ "$IPFSD_TEST_RELEASE" =~ go-ipfs-([0-9].[0-9]+.[0-9]+) ]]; then
        VER="${BASH_REMATCH[1]}"

        curl -o go-ipfs.tar.gz \
          https://dist.ipfs.tech/go-ipfs/v${VER}/go-ipfs_v${VER}_linux-amd64.tar.gz
        tar -xvf go-ipfs.tar.gz && cp go-ipfs/ipfs /opt/ipfs/bin
      fi

      # Match kubo version number (will work with x.xx.xx)
      if [[ "$IPFSD_TEST_RELEASE" =~ kubo-([0-9].[0-9]+.[0-9]+) ]]; then
        VER="${BASH_REMATCH[1]}"

        curl -o kubo.tar.gz \
          https://dist.ipfs.tech/kubo/v${VER}/kubo_v${VER}_linux-amd64.tar.gz
        tar -xvf kubo.tar.gz && cp kubo/ipfs /opt/ipfs/bin
      fi

    - ipfs version

    - python3 -m pip install --upgrade pip
    - python3 -m venv venv

    - source venv/bin/activate
    - pip install wheel
    - pip install -r requirements-dev.txt
    - pip install -r requirements.txt

    - flake8 aioipfs tests --count --select=E9,F63,F7,F82 --show-source --statistics
    - python3 setup.py build install

    - pytest -v -s tests
    - python setup.py sdist bdist_wheel

  artifacts:
    paths:
      - dist/*.whl

pypi:
  stage: pypi
  cache: {}
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[publish\]/
      when: always
  before_script:
    - apt-get update
    - apt-get install -qy --force-yes python3-pip python3-venv
  script:
    - pip install wheel
    - pip install -r requirements-dev.txt
    - pip install -r requirements.txt
    - pip install -U twine

    - python3 setup.py sdist bdist_wheel

    - twine upload dist/*
